package com.example.videoupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class VideoUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideoUploadApplication.class, args);
    }

    @GetMapping("/youtube-data-api")
    public Object youtube(
            @RequestParam String key,
                          @RequestParam String part,
                          @RequestParam String q,
                          @RequestParam String type,
                          @RequestParam String maxResults){

        String url = "https://www.googleapis.com/youtube/v3/search?part={part}&maxResults={maxResults}&q={q}&key={key}";

        Map<String, String> params = new HashMap<>();
        params.put("key",key);
        params.put("part", part);
        params.put("q", q);
        params.put("type", type);
        params.put("maxResults", maxResults);

        RestTemplate rest = new RestTemplate();


        return rest.getForObject(url, Object.class,params);
    }

    @PostMapping("/post-video")
    public void postVideo(@RequestParam String key,
                          @RequestParam String part){

        String url = "https://www.googleapis.com/youtube/v3/search?part={part}&key={key}";

        RestTemplate rest = new RestTemplate();


//        File mediaFile = new File("YOUR_FILE");
//        InputStreamContent mediaContent =
//                new InputStreamContent("application/octet-stream",
//                        new BufferedInputStream(new FileInputStream(mediaFile)));
//        mediaContent.setLength(mediaFile.length());
//
//        // Define and execute the API request
//        YouTube.Videos.Insert request = youtubeService.videos()
//                .insert("snippet,status", video, mediaContent);
//        Video response = request.execute();
//        System.out.println(response);

    }

}
